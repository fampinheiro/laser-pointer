# Laser pointer

Laser pointer is composed by 3 components:
```
    +--------+     +--------+     +-------------+ 
    | webapp | <-> | server | <-> | android app |
    +--------+     +--------+     +-------------+
```
The main purpose is to control a pointer in the webapp based on the motion of an android phone.

[socketio](http://socket.io/) is used to pass sensor information to the server that sends it to the web app.
Using the accelerometer, the magnetic field and the gyroscope to try to stabilize the sensor readings it is possible to mao the phone movement to screen movements.
When the user move the phone along the X axis the point move vertically and when the user move the phone along the Z axis the point move horizontally.

The webapp and the server use the node module [socketio](https://npmjs.org/package/socket.io).  
The android app uses [socket.io-java-client](https://github.com/Gottox/socket.io-java-client).

## Run locally

To run locally just clone the repository and run `server.js` with `node`.

``` bash 
git clone git://github.com/fampinheiro/laser-pointer.git
cd laser-pointer
node server.js
``` 
By default this will run on http://localhost:5000
