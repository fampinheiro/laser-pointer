var http = require('http'),
	express = require('express'),
	socketio = require('socket.io')

var app = express();
app.configure(function () {
	app.use(express.static([__dirname, 'public'].join('/')));
});

var server = http.createServer(app);
var io = socketio.listen(server);


// this configuration is needed to host 
// socketio in heroku
io.configure(function () { 
  if (process.env.PORT) {
    io.set("transports", ["xhr-polling"]); 
    io.set("polling duration", 10); 
  }
});

io.sockets.on('connection', function (socket) {
  socket.on('message', function (msg) {
    console.log('>', msg);
    socket.broadcast.emit('message', msg);
  });
});

var port = process.env.PORT || 5000;
server.listen(port, function () {
  console.log('%s listening at %d in %s',
    server.address().address,
    server.address().port,
    app.settings.env);
});


