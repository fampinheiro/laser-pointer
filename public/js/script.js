var canvas = document.createElement('canvas');
document.body.insertBefore(
  canvas, document.body.firstChild);

var ctx = canvas.getContext('2d');

var x = 0;
var y = 0;

function drawPointer(context, x, y) {
  context.translate(
    originX + multiplier * x,
    originY + multiplier * y);

  context.beginPath();
  context.strokeStyle = 'red';
  context.lineWidth = 2;
  context.moveTo(0,-4);
  context.lineTo(0,4);
  context.moveTo(-4,0);
  context.lineTo(4,0);
  context.stroke();
}

var multiplier;
var originX, originY;
function resizeCanvas() {
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;  

  originX = canvas.width / 2;
  originY = canvas.height / 2;

  multiplier = originX > originY ? originX : originY;
  drawPointer(ctx, x, y);
}

$(window).on('resize', function () {
  resizeCanvas();
});
resizeCanvas();

$(function(){
  var socket = io.connect();
  socket.on('connect', function () {
    $('#incoming').prepend($('<li>')
      .text('connected')
      .addClass('shaded'));
    socket.on('message', function(message) {
      $('#incoming').prepend($('<li>')
        .text(JSON.stringify(message)));

      if (message.x !== undefined 
        && message.y !== undefined) {
        x = message.x;
        y = message.y;
      resizeCanvas();
    }
  });
    socket.on('disconnect', function() {
      $('#incoming').prepend($('<li>')
        .text('disconnected')
        .addClass('shaded'));
    });
  });
});
